/*globals $, alert*/

/*
function animate(x) {
    "use strict";
    if (x === 1) {
        setInterval(function () {
            $("#img-f1-f").animate({left: 0, top: 20, opacity: 1}, 500)
                .animate({left: 20, top: 0}, 500);
        }, 1000);
    } else {
        setInterval(function () {
            $("#img-f" + x + "-f").animate({top: 15}, 500)
                .animate({top: 5}, 500);
        }, 1000);
    }
}
*/

function frameLeave() {
    "use strict";
    $(".shake").hide();
    $(".fade").hide();
    $(".bottomin").css({"top": "50px", "opacity": "0"});
    $(".rightin").css("left", "800px");
    $(".leftin").css("left", "-800px");
}

function frameLoad(x) {
    "use strict";
    frameLeave();
    var f = $("#section" + x);
    if (x === 1) {
        f.find(".fade").each(function (index) {
            $(this).delay(500 * index + 500).fadeIn(500);
        });
        f.find(".shake").fadeIn(500);
        f.find(".bottomin").animate({top: 0, opacity: 1}, 500);
    }
    if (x <= 5 && x > 1) {
        f.find(".shake").each(function (index) {
            $(this).fadeIn(500);
        });
        f.find(".leftin").each(function (index) {
            $(this).delay(200 * index + 500).animate({left: 0}, 500);
        });
        f.find(".rightin").each(function (index) {
            $(this).delay(200 * index + 500).animate({left: 0}, 500);
        });
    }
    if (x <= 7 && x > 5) {
        f.find(".fade").each(function (index) {
            $(this).delay(300 * index).fadeIn(500);
        });
        f.find(".rightin").delay(500).animate({left: 0}, 500);
        f.find(".shake").delay(2200).fadeIn(500);
    }
    if (x <= 9 && x > 7) {
        f.find(".fade").each(function (index) {
            $(this).delay(200 * index).fadeIn(500);
        });
        f.find(".rightin").delay(400).animate({left: 0}, 500);
        f.find(".leftin").delay(400).animate({left: 0}, 500);
    }
    if (x === 10 || x === 12) {
        f.find(".fade").each(function (index) {
            $(this).delay(500 * index).fadeIn(500);
        });
    }
    if (x === 11) {
        f.find(".fade").fadeIn(500);
        f.find(".bottomin").animate({top: 0, opacity: 1}, 500);
        f.find(".leftin").each(function (index) {
            $(this).delay(200 * index).animate({left: 0}, 500);
        });
    }
}

$(document).ready(function () {
    "use strict";
    frameLoad(1);
    $("#fullpage").fullpage({
        afterLoad: function (anchorLink, index) {
            frameLoad(index);
        }
    });
});

